import 'package:flutter/foundation.dart';
import 'package:simple_model_state/simple_model_state.dart';

class SplashPageModel extends BaseModel {
  final Function() _onDone;

  SplashPageModel({
    @required Function() onDone,
  }) : _onDone = onDone {
    _init();
  }

  Future<void> _init() async {
    await Future<void>.delayed(Duration.zero);
    await _onDone();
  }
}
