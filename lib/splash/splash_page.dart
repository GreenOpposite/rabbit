import 'package:flutter/material.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'splash_page_model.dart';
import '../home/home_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  SplashPageModel _model;

  @override
  void initState() {
    super.initState();
    _model = SplashPageModel(
      onDone: () => Navigator.of(context).pushReplacement(
        _SplashPageCustomRoute<void>(
          builder: (context) => HomePage(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashPageModel>(
      model: _model,
      builder: (context, model, child) {
        return Scaffold(
          body: Center(
            child: Text('Hold on :P'),
          ),
        );
      },
    );
  }
}

class _SplashPageCustomRoute<T> extends MaterialPageRoute<T> {
  _SplashPageCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Duration get transitionDuration => Duration.zero;
}
