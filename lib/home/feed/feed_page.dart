import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rabbit/utilities/listing_type_extension.dart';
import 'package:rabbit/utilities/string_extension.dart';
import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

import '../../link/detailed_link_page.dart';
import '../../widgets/link_card.dart';
import 'feed_page_model.dart';

class FeedPage extends StatefulWidget {
  const FeedPage({Key key}) : super(key: key);

  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage>
    with AutomaticKeepAliveClientMixin<FeedPage> {
  FeedPageModel _model;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_scrollListener);
    // TODO: Load preferences when available
    _model = FeedPageModel(
      redditClient: Provider.of(context, listen: false),
      listingType: ListingType.best,
    );
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BaseWidget<FeedPageModel>(
      model: _model,
      builder: (context, model, child) {
        return Scaffold(
          body: SafeArea(
            child: Center(
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 250),
                child: model.state.when(
                  loading: () => Text('Getting your reddits'),
                  success: (links) => _FeedList(
                    scrollController: _scrollController,
                    loadingMore: false,
                    links: links,
                  ),
                  gettingMore: (links) => _FeedList(
                    scrollController: _scrollController,
                    loadingMore: true,
                    links: links,
                  ),
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: FaIcon(FontAwesomeIcons.sort),
            onPressed: () {
              showModalBottomSheet<String>(
                  context: context,
                  builder: (context) {
                    return Container(
                      child: Wrap(
                        children: getSortOptions(),
                      ),
                    );
                  });
            },
          ),
        );
      },
    );
  }

  List<Widget> getSortOptions() {
    return ListingType.values.map((listingType) {
      return ListTile(
        leading: FaIcon(listingType.icon),
        title: Text(
          listingType.path.toString().capitalize(),
          style: TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        onTap: () {
          _model.changeListingType(listingType);
          Navigator.pop(context);
        },
      );
    }).toList();
  }

  @override
  bool get wantKeepAlive => true;

  void _scrollListener() {
    const preloadOffset = 1000;
    if (_scrollController.offset >=
            (_scrollController.position.maxScrollExtent - preloadOffset) &&
        !_scrollController.position.outOfRange) {
      _model.getMore();
    }
  }
}

class _FeedList extends StatelessWidget {
  final List<Link> links;
  final ScrollController scrollController;
  final bool loadingMore;

  const _FeedList({
    Key key,
    @required this.links,
    @required this.scrollController,
    @required this.loadingMore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: ListView(
        controller: scrollController,
        children: <Widget>[
          for (final link in links)
            Stack(
              children: <Widget>[
                LinkCard(
                  link: link,
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute<void>(
                      builder: (context) => DetailedLinkPage(
                        link: link,
                        allLinks: links,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(loadingMore ? 'Hold on...' : 'The end'),
            ),
          ),
        ],
      ),
    );
  }
}
