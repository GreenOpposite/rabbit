import 'package:flutter/foundation.dart';
import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'feed_page_state.dart';

class FeedPageModel extends BaseModel {
  final RedditClient _redditClient;

  FeedPageState _state = FeedPageState.loading();

  FeedPageState get state => _state;

  ListingType _selectedListingType = ListingType.best;

  FeedPageModel({@required RedditClient redditClient, ListingType listingType})
      : _redditClient = redditClient {
    _init();
    _selectedListingType = listingType;
  }

  void changeListingType(ListingType listingType) {
    _selectedListingType = listingType;
    _init();
  }

  Future<void> _init() async {
    try {
      final feed = await _redditClient.getFeed(_selectedListingType);
      _state = FeedPageState.success(links: feed.children);
    } catch (e, st) {
      print(st);
      // TODO: Have an error state
    }
    notifyListeners();
  }

  Future<void> getMore() async {
    await state.maybeWhen(
      success: (links) async {
        _state = FeedPageState.gettingMore(links: links);
        notifyListeners();
        try {
          final feed = await _redditClient.getFeed(
            _selectedListingType,
            after: links.last.name,
          );
          // feedItems.addAll(feed.children);
          _state = FeedPageState.success(links: [...links, ...feed.children]);
        } catch (e, st) {
          print(st);
          // _state = FeedPageState.error;
        }
        notifyListeners();
      },
      orElse: () => null,
    );
  }
}
