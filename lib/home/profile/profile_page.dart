import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'profile_page_model.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  ProfilePageModel _model;

  @override
  void initState() {
    super.initState();
    _model = ProfilePageModel(
      redditClient: Provider.of(context, listen: false),
      linksHandler: Provider.of(context, listen: false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProfilePageModel>(
      model: _model,
      builder: (context, model, child) {
        return Scaffold(
          body: SafeArea(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: model.state.when(
                      loading: () => Text('Wait for it...'),
                      signedOut: () => _LoginButton(onPressed: model.login),
                      signingIn: () => _LoginButton(onPressed: null),
                      signedIn: () => Text('Fetching user'),
                      profile: (user) => GestureDetector(
                        onTap: model.logout,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('${user.name}'),
                            Text('(tap to log out)'),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  final void Function() onPressed;

  const _LoginButton({Key key, @required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onPressed,
      child: Text('Login'),
    );
  }
}
