import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rednit/rednit.dart';

part 'detailed_link_page_state.freezed.dart';

@freezed
abstract class DetailedLinkPageState with _$DetailedLinkPageState {
  const factory DetailedLinkPageState.loading({
    @required Link link,
  }) = DetailedLinkPageLoadingState;

  const factory DetailedLinkPageState.success({
    @required Link link,
    @required List<Comment> comments,
  }) = DetailedLinkPageSuccessState;
}
