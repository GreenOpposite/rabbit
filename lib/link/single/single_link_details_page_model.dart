import 'package:flutter/foundation.dart';
import 'package:rabbit/link/single/single_link_details_page_state.dart';
import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

class SingleLinkDetailsPageModel extends BaseModel {
  final RedditClient _redditClient;
  final Link link;

  SingleLinkDetailsPageState _state;
  SingleLinkDetailsPageState get state => _state;

  SingleLinkDetailsPageModel({
    @required this.link,
    @required RedditClient redditClient,
  })  : _redditClient = redditClient,
        _state = SingleLinkDetailsPageState.loading(link: link) {
    _init();
  }

  Future<void> _init() async {
    final commentListing = await _redditClient.getComments(state.link.id);
    state.when(
      loading: (link) {
        _state = SingleLinkDetailsPageState.success(
          link: state.link,
          comments: commentListing.children,
        );
      },
      success: (link, comments) {
        _state = SingleLinkDetailsPageState.success(
          link: state.link,
          comments: [...comments, ...commentListing.children],
        );
      },
    );

    notifyListeners();
  }
}
