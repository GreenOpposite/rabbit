import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rednit/rednit.dart';

part 'single_link_details_page_state.freezed.dart';

@freezed
abstract class SingleLinkDetailsPageState with _$SingleLinkDetailsPageState {
  const factory SingleLinkDetailsPageState.loading({
    @required Link link,
  }) = SingleLinkDetailsPageLoadingState;

  const factory SingleLinkDetailsPageState.success({
    @required Link link,
    @required List<Comment> comments,
  }) = SingleLinkDetailsPageSuccessState;
}
