import 'package:flutter/foundation.dart';
import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'user_page_state.dart';

class UserPageModel extends BaseModel {
  final RedditClient _redditClient;

  UserPageState _state;
  UserPageState get state => _state;

  UserPageModel({
    @required RedditClient redditClient,
    @required User user,
  })  : _redditClient = redditClient,
        _state = UserPageState.loading(user: user) {
    _init();
  }

  Future<void> _init() async {
    final user = await _redditClient.getUser(_state.user.name);
    final posts = await _redditClient.getUserSubmissions(_state.user.name);
    final comments = await _redditClient.getUserComments(_state.user.name);
    _state = UserPageState.success(
      user: user,
      comments: comments.children,
      posts: posts.children,
    );
    notifyListeners();
  }

  Future<void> getMoreLinks(int limit) async {
    _state = await state.when(
      loading: (user) async {
        final fetchedPosts = await _redditClient.getUserSubmissions(user.name);
        return UserPageState.success(
          user: user,
          comments: [],
          posts: fetchedPosts.children,
        );
      },
      success: (user, comments, posts) async {
        final fetchedPosts = await _redditClient.getUserSubmissions(
          user.name,
          after: posts.last.id,
        );
        return UserPageState.success(
          user: user,
          comments: comments,
          posts: posts..addAll(fetchedPosts.children),
        );
      },
    );
  }
}
