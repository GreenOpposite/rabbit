import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rabbit/user/overview/user_overview_popup_model.dart';
import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

import '../user_page.dart';
import '../../utilities/date_format.dart';

class UserOverviewPopup extends StatefulWidget {
  final String username;

  const UserOverviewPopup({
    Key key,
    @required this.username,
  }) : super(key: key);

  @override
  _UserOverviewPopupState createState() => _UserOverviewPopupState();
}

class _UserOverviewPopupState extends State<UserOverviewPopup>
    with SingleTickerProviderStateMixin {
  UserOverviewPopupModel _model;

  @override
  void initState() {
    super.initState();
    _model = UserOverviewPopupModel(
      redditClient: context.read(),
      username: widget.username,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<UserOverviewPopupModel>(
      model: _model,
      builder: (context, model, child) {
        return Center(
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              elevation: 4.0,
              child: AnimatedSize(
                vsync: this,
                duration: const Duration(milliseconds: 350),
                curve: Curves.linearToEaseOut,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    model.state.when(
                      loading: (username) => _LoadingBody(username: username),
                      success: (user) => _SuccessBody(user: user),
                      error: (dynamic error) => _ErrorBody(error: error),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class _ErrorBody extends StatelessWidget {
  final dynamic error;

  const _ErrorBody({
    Key key,
    @required this.error,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      error.toString(),
      style: Theme.of(context).textTheme.headline6,
    );
  }
}

class _SuccessBody extends StatelessWidget {
  final User user;

  const _SuccessBody({
    Key key,
    @required this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      key: ValueKey<String>('SuccessWidget'),
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            user.name,
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    'Karma',
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  Text(
                    '${user.commentKarma}',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Age',
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  Text(
                    DateTime.now().difference(user.created).readableShort,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ],
              ),
            ],
          ),
        ),
        ListTile(
          leading: FaIcon(FontAwesomeIcons.userCircle),
          title: Text('View Profile'),
          onTap: () => Navigator.of(context).pushReplacement(
            MaterialPageRoute<void>(
              builder: (context) => UserPage(user: user),
            ),
          ),
        ),
      ],
    );
  }
}

class _LoadingBody extends StatelessWidget {
  final String username;

  const _LoadingBody({
    Key key,
    @required this.username,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      key: ValueKey<String>('LoadingWidget'),
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            username,
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
      ],
    );
  }
}
