import 'package:flutter/foundation.dart';
import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'user_overview_popup_state.dart';

class UserOverviewPopupModel extends BaseModel {
  final RedditClient _redditClient;

  UserOverviewPopupState _state;
  UserOverviewPopupState get state => _state;

  UserOverviewPopupModel({
    @required RedditClient redditClient,
    @required String username,
  })  : _redditClient = redditClient,
        _state = UserOverviewPopupState.loading(username: username) {
    _init();
  }

  Future<void> _init() async {
    final username = _state.maybeWhen(
      loading: (username) => username,
      orElse: () => null,
    );

    print('Loading user $username');
    final user = await _redditClient.getUser(username);

    print('Loaded user $user');

    _state = user == null
        ? UserOverviewPopupState.error(error: 'null')
        : UserOverviewPopupState.success(user: user);
    notifyListeners();
  }
}
