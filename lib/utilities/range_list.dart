extension RangeList on int {
  List<int> to(int other) {
    if (this < other) {
      return List.generate(other - this + 1, (i) => this + i);
    } else if (this > other) {
      return List.generate(this - other + 1, (i) => this - i);
    }
    return [this];
  }
}
