import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rednit/rednit.dart';

extension ListingTypeIcons on ListingType {
  IconData get icon {
    switch (this) {
      case ListingType.hot:
        return FontAwesomeIcons.fire;
      case ListingType.recent:
        return FontAwesomeIcons.solidClock;
      case ListingType.best:
        return FontAwesomeIcons.solidThumbsUp;
      case ListingType.controversial:
        return FontAwesomeIcons.dumpsterFire;
      case ListingType.top:
        return FontAwesomeIcons.solidArrowAltCircleUp;
      default:
        throw Exception('This should never happen');
    }
  }
}
