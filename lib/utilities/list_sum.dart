extension ListSum<T extends num> on List<T> {
  T sum() => reduce((a, b) => (a + b) as T);
}
