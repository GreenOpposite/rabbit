import 'package:shared_preferences/shared_preferences.dart';
import 'package:rednit/rednit.dart';

class SharedPreferencesTokenStore extends TokenStore {
  static const _accessTokenKey = 'reddit/access_token';
  static const _refreshTokenKey = 'reddit/refresh_token';
  static const _accessTokenExpirationKey = 'reddit/access_token_expiration';
  static const _deviceIdKey = 'reddit/device_id';

  final SharedPreferences _preferences;

  SharedPreferencesTokenStore({
    SharedPreferences sharedPreferences,
  }) : _preferences = sharedPreferences;

  @override
  String get accessToken => _preferences.getString(_accessTokenKey);

  @override
  String get refreshToken => _preferences.getString(_refreshTokenKey);

  @override
  DateTime get accessTokenExpiration {
    final expirationMillis = _preferences.getInt(_accessTokenExpirationKey);
    return DateTime.fromMillisecondsSinceEpoch(expirationMillis ?? 0);
  }

  @override
  String get deviceId => _preferences.getString(_deviceIdKey);

  @override
  Future<void> setAccessToken(String accessToken) async {
    await _preferences.setString(_accessTokenKey, accessToken);
  }

  @override
  Future<void> setRefreshToken(String refreshToken) async {
    await _preferences.setString(_refreshTokenKey, refreshToken);
  }

  @override
  Future<void> setAccessTokenExpiration(DateTime expirationTime) async {
    await _preferences.setInt(
        _accessTokenExpirationKey, expirationTime.millisecondsSinceEpoch);
  }

  @override
  Future<void> setDeviceId(String deviceId) async {
    await _preferences.setString(_deviceIdKey, deviceId);
  }

  @override
  Future<void> removeAccessToken() async {
    await _preferences.remove(_accessTokenKey);
  }

  @override
  Future<void> removeRefreshToken() async {
    await _preferences.remove(_refreshTokenKey);
  }

  @override
  Future<void> removeAccessTokenExpiration() async {
    await _preferences.remove(_accessTokenExpirationKey);
  }
}
