# Rabbit

A Reddit client made in Flutter.

## Getting Started

To try it out yourself, you first need to have a Reddit account and create a Reddit application at https://www.reddit.com/prefs/apps/. This will give you access to a Client ID, which is required to use Reddit's APIs.

Then you have to create the file `lib/secrets/reddit_constants.dart` and declare the class below, with your own client ID and username.

```dart
class RedditConstants {
  static const clientId = '<YOUR_CLIENT_ID>';
  static const creatorUsername = '<YOUR_REDDIT_USERNAME>';
  static const redirectUri = 'nittiet://rabbit/login-redirect';
}
```

After that you should be able to compile and use the app.

## Screenshots

<img src="screenshots/front-page.jpeg" height="300"/>
<img src="screenshots/details.jpeg" height="300"/>
<img src="screenshots/subreddit.jpeg" height="300"/>

## Dependencies

This app uses the Reddit API Wrapper [rednit](https://gitlab.com/alfonsinbox/rednit).

## Contributing

Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
For more info, read the [contributing](/CONTRIBUTING.md) guide.

## Progress

- [ ] WIP: Add documentation

### Reading

- [x] Front page
- [x] Comments for submission
- [x] Sideways navigation between submissions
- [x] Infinite scrolling on front page
- [ ] Infinite scrolling on subreddit page
- [ ] Infinite swiping between submission
- [ ] Get more replies
- [ ] Separate page for user info
- [ ] Sort comments and submissions by hot/new/etc.
- [ ] Open links

### Actions

- [ ] Up- / downvote submissions, comments
- [ ] Make comments / replies

### Details

- [ ] Show details on comments
  - [ ] Rewards
  - [ ] Whether author is OP, Admin, etc.
  - [ ] User badges
- [ ] Animate comment collapse
- [ ] Parse markdown (or alt. use HTML)
